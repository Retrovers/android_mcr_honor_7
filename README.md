# MoDaCo Custom ROM for the Honor 7

This repository contains MoDaCo Custom ROM for the Honor 7 device, together with the stock ROM templates.

The support topic for this ROM can be found at [the MoDaCo Forums](http://www.modaco.com/forums/topic/377036-r41-modaco-custom-rom-honor-7-plk-l01/).
