cd updates/app
find * -maxdepth 0 -type d -exec rm -r ../../system/app/{} \; -exec mv {} ../../system/app/ \;
cd -
cd updates/priv-app
find * -maxdepth 0 -type d -exec rm -r ../../system/priv-app/{} \; -exec mv {} ../../system/priv-app/ \;
cd -
cd updates/delapp
find * -maxdepth 0 -type d -exec rm -r ../../system/delapp/{} \; -exec mv {} ../../system/delapp/ \;
cd -
cd updates/cust
find * -maxdepth 0 -type d -exec rm -r ../../cust/preinstalled/public/app/{} \; -exec mv {} ../../cust/preinstalled/public/app/ \;
cd -
